--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.detailscommande DROP CONSTRAINT detailscommande_idcommande_fkey;
ALTER TABLE ONLY public.detailscommande DROP CONSTRAINT detailscommande_idarticle_fkey;
ALTER TABLE ONLY public.article DROP CONSTRAINT article_idcategorie_fkey;
ALTER TABLE ONLY public.tables DROP CONSTRAINT tables_url_key;
ALTER TABLE ONLY public.tables DROP CONSTRAINT tables_pkey;
ALTER TABLE ONLY public.tables DROP CONSTRAINT tables_nom_key;
ALTER TABLE ONLY public.detailscommande DROP CONSTRAINT detailscommande_pkey;
ALTER TABLE ONLY public.commande DROP CONSTRAINT commande_pkey;
ALTER TABLE ONLY public.categorie DROP CONSTRAINT categorie_position_key;
ALTER TABLE ONLY public.categorie DROP CONSTRAINT categorie_pkey;
ALTER TABLE ONLY public.categorie DROP CONSTRAINT categorie_nom_key;
ALTER TABLE ONLY public.article DROP CONSTRAINT article_pkey;
ALTER TABLE ONLY public.article DROP CONSTRAINT article_nom_key;
ALTER TABLE public.tables ALTER COLUMN idtable DROP DEFAULT;
ALTER TABLE public.detailscommande ALTER COLUMN iddetailscommande DROP DEFAULT;
ALTER TABLE public.commande ALTER COLUMN idcommande DROP DEFAULT;
ALTER TABLE public.categorie ALTER COLUMN idcategorie DROP DEFAULT;
ALTER TABLE public.article ALTER COLUMN idarticle DROP DEFAULT;
DROP SEQUENCE public.tables_idtable_seq;
DROP TABLE public.tables;
DROP SEQUENCE public.detailscommande_iddetailscommande_seq;
DROP TABLE public.detailscommande;
DROP SEQUENCE public.commande_idcommande_seq;
DROP TABLE public.commande;
DROP SEQUENCE public.categorie_idcategorie_seq;
DROP TABLE public.categorie;
DROP SEQUENCE public.article_idarticle_seq;
DROP TABLE public.article;
SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: article; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.article (
    idarticle integer NOT NULL,
    idcategorie integer,
    nom character varying(100) NOT NULL,
    description character varying(500),
    prix double precision,
    visibilite boolean,
    "position" integer NOT NULL
);


ALTER TABLE public.article OWNER TO root;

--
-- Name: article_idarticle_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.article_idarticle_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.article_idarticle_seq OWNER TO root;

--
-- Name: article_idarticle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.article_idarticle_seq OWNED BY public.article.idarticle;


--
-- Name: categorie; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.categorie (
    idcategorie integer NOT NULL,
    nom character varying(100) NOT NULL,
    visibilite boolean,
    "position" integer NOT NULL
);


ALTER TABLE public.categorie OWNER TO root;

--
-- Name: categorie_idcategorie_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.categorie_idcategorie_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categorie_idcategorie_seq OWNER TO root;

--
-- Name: categorie_idcategorie_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.categorie_idcategorie_seq OWNED BY public.categorie.idcategorie;


--
-- Name: commande; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.commande (
    idcommande integer NOT NULL,
    numtable integer NOT NULL,
    status integer NOT NULL,
    commentaire character varying(500)
);


ALTER TABLE public.commande OWNER TO root;

--
-- Name: commande_idcommande_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.commande_idcommande_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commande_idcommande_seq OWNER TO root;

--
-- Name: commande_idcommande_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.commande_idcommande_seq OWNED BY public.commande.idcommande;


--
-- Name: detailscommande; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.detailscommande (
    iddetailscommande integer NOT NULL,
    idcommande integer,
    idarticle integer NOT NULL,
    idquantite integer NOT NULL
);


ALTER TABLE public.detailscommande OWNER TO root;

--
-- Name: detailscommande_iddetailscommande_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.detailscommande_iddetailscommande_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detailscommande_iddetailscommande_seq OWNER TO root;

--
-- Name: detailscommande_iddetailscommande_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.detailscommande_iddetailscommande_seq OWNED BY public.detailscommande.iddetailscommande;


--
-- Name: tables; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.tables (
    idtable integer NOT NULL,
    nom character varying(10) NOT NULL,
    url character varying(50)
);


ALTER TABLE public.tables OWNER TO root;

--
-- Name: tables_idtable_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.tables_idtable_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tables_idtable_seq OWNER TO root;

--
-- Name: tables_idtable_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE public.tables_idtable_seq OWNED BY public.tables.idtable;


--
-- Name: article idarticle; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.article ALTER COLUMN idarticle SET DEFAULT nextval('public.article_idarticle_seq'::regclass);


--
-- Name: categorie idcategorie; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.categorie ALTER COLUMN idcategorie SET DEFAULT nextval('public.categorie_idcategorie_seq'::regclass);


--
-- Name: commande idcommande; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.commande ALTER COLUMN idcommande SET DEFAULT nextval('public.commande_idcommande_seq'::regclass);


--
-- Name: detailscommande iddetailscommande; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.detailscommande ALTER COLUMN iddetailscommande SET DEFAULT nextval('public.detailscommande_iddetailscommande_seq'::regclass);


--
-- Name: tables idtable; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tables ALTER COLUMN idtable SET DEFAULT nextval('public.tables_idtable_seq'::regclass);


--
-- Data for Name: article; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.article (idarticle, idcategorie, nom, description, prix, visibilite, "position") FROM stdin;
\.


--
-- Data for Name: categorie; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.categorie (idcategorie, nom, visibilite, "position") FROM stdin;
\.


--
-- Data for Name: commande; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.commande (idcommande, numtable, status, commentaire) FROM stdin;
\.


--
-- Data for Name: detailscommande; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.detailscommande (iddetailscommande, idcommande, idarticle, idquantite) FROM stdin;
\.


--
-- Data for Name: tables; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.tables (idtable, nom, url) FROM stdin;
\.


--
-- Name: article_idarticle_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.article_idarticle_seq', 1, false);


--
-- Name: categorie_idcategorie_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.categorie_idcategorie_seq', 1, false);


--
-- Name: commande_idcommande_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.commande_idcommande_seq', 1, false);


--
-- Name: detailscommande_iddetailscommande_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.detailscommande_iddetailscommande_seq', 1, false);


--
-- Name: tables_idtable_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.tables_idtable_seq', 1, false);


--
-- Name: article article_nom_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_nom_key UNIQUE (nom);


--
-- Name: article article_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_pkey PRIMARY KEY (idarticle);


--
-- Name: categorie categorie_nom_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.categorie
    ADD CONSTRAINT categorie_nom_key UNIQUE (nom);


--
-- Name: categorie categorie_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.categorie
    ADD CONSTRAINT categorie_pkey PRIMARY KEY (idcategorie);


--
-- Name: categorie categorie_position_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.categorie
    ADD CONSTRAINT categorie_position_key UNIQUE ("position");


--
-- Name: commande commande_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.commande
    ADD CONSTRAINT commande_pkey PRIMARY KEY (idcommande);


--
-- Name: detailscommande detailscommande_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.detailscommande
    ADD CONSTRAINT detailscommande_pkey PRIMARY KEY (iddetailscommande);


--
-- Name: tables tables_nom_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tables
    ADD CONSTRAINT tables_nom_key UNIQUE (nom);


--
-- Name: tables tables_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tables
    ADD CONSTRAINT tables_pkey PRIMARY KEY (idtable);


--
-- Name: tables tables_url_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tables
    ADD CONSTRAINT tables_url_key UNIQUE (url);


--
-- Name: article article_idcategorie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_idcategorie_fkey FOREIGN KEY (idcategorie) REFERENCES public.categorie(idcategorie) ON DELETE CASCADE;


--
-- Name: detailscommande detailscommande_idarticle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.detailscommande
    ADD CONSTRAINT detailscommande_idarticle_fkey FOREIGN KEY (idarticle) REFERENCES public.article(idarticle);


--
-- Name: detailscommande detailscommande_idcommande_fkey; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.detailscommande
    ADD CONSTRAINT detailscommande_idcommande_fkey FOREIGN KEY (idcommande) REFERENCES public.commande(idcommande) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

