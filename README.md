# Installation du projet from scratch

## La base de données

Le dump dumpmodel.sql contient les tables vides.

Les index ont été mis à 0 avec la commande:

```sql
TRUNCATE TABLE categorie RESTART IDENTITY CASCADE;
```

Cette requête a été executée sur chaque table.

### 1 ère étape

On peut restaurer cette BDD via  la commande:

```bash
cat dumpmodel.sql | docker exec -i pg_container psql -U root -d modeldb
```
### 2 ème étape

Maintenant que nous avons un modèle, nous allons créer notre première base de données

```bash
docker exec -it pg_container psql -U root -d restaurant -c "CREATE DATABASE resto2 WITH TEMPLATE modeldb;"
```

On va pouvoir ensuite créer un utilisateur ainsi que son mot de passe associé avec les commandes suivantes:

```bash
docker exec -it pg_container psql -U root -d restaurant -c "CREATE USER resto2 SUPERUSER PASSWORD '98giu2b3eqd98gub98gub3q';"
docker exec -it pg_container psql -U root -d restaurant -c "GRANT ALL PRIVILEGES ON DATABASE resto2 TO resto2;"
```

Donc, on se retrouvera avec la chaine de connexion suivante dans la variable DATABASE_URL du fichier app.py (donc dans le fichier docker-compose.yml)

```
- DATABASE_URL=postgresql://resto2:98giu2b3eqd98gub98gub3q@pg_container/resto2
```

L'interêt est que chaque utilisateur\restaurateur aura sa propre chaine de connexion à sa database (plus secure)

## Le parcours Devops

Comment ca marche ?

Le restaurateur indique le nom de son restaurant (avec des caractères uniquement alphanumériques, sans accent, tirets, symboles...)

Ex: lecafedelapaix

Dans un premier temps, on vérifie la disponibilité du sous-domaine lecafedelapaix.novation.menu

Si le sous-domaine n'est pas libre, on invite le restaurateur a choisir un autre nom... Si le domaine est libre, on va le créer, plusieurs possibilté s'offrent à nous.

- Le domaine est hébérgé chez OVH, on peut créer un sous-domaine via une api [Les API OVH](https://api.ovh.com/console/#/domain/zone/%7BzoneName%7D/refresh#POST)
- On delegue la résolution DNS à un serveur DNS qu'on aura containeuriser
- On utilise un service DNS du provider cloud qu'on aura choisi

La problématique qu'on pourrait rencontrer est le temps de propagation DNS bien que pour un sous-domaine cela doit être bien plus rapide que pour un domaine (A tester). C'est surtout au niveau du certificat https que l'on risque de rencontrer des difficultés. Combien de temps faudra-t-il de temps pour que Lets Encrypt valide ce nouveau sous-domaine?

Si la propagation n'est pas immédiate, on execute un crontab qui vérifie si la propagation est OK et dès que c'est le cas, on envoie un mail avec toutes les infos au restaurateur.

En parallèle, on créé une BDD avec le nom qu'a spécifié le restaurateur, on reprend la commande de la première partie.

```bash
docker exec -it pg_container psql -U root -d restaurant -c "CREATE DATABASE lecafedelapaix WITH TEMPLATE modeldb;"
```

On execute les autres commandes vues dans la première partie, on pourra générer le mot de passe en random et éventuellement utiliser le même pour la BDD et l'authentification.

On génère un docker-compose.yml pour le dashboard et le menu avec un fichier .env qui contiendrait le nom choisi par le restaurateur (ex: lecafedelapaix), le mot de passe, l'URL de connexion à la BDD ...

Ensuite, il reste à regarder comment fonctionne Authelia avec une base de données. Peut-on faire un update avec le nom (ex: lecafedelapaix), le mot de passe, le nom du site sur lequel on veut s'authentifier ?. On reste dans la logique de tout scripter de A à Z ...

## Lectures utiles

[SSO with Traefik and Kubernetes](https://medium.com/@TCheronneau/sso-with-traefik-and-kubernetes-d008f9a1328a)

[Exemple de configuration Traefik](https://github.com/faritvidal/Config-Traefik-2.2.X)


[Configuration des secrets dans Authelia](https://www.authelia.com/docs/configuration/secrets.html)